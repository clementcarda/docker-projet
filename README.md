# Projet Docker
**contributeurs** : BLANCHET Noémie, CARDARELLI Clément, ESTIENNE Jon

## Prérequis
Docker version 20.10.5+
Docker-compose version 1.28.5+

## Lancer le projet
modifier et renommer le fichier ./config/.env.exemple en ./config/.env

Afin que traefik puisse rediriger correctement les requètes elles doivent être faite avec les noms d'hôtes des différents services, il faut donc penser à les ajouter sur son DNS ou son fichier hosts. chaque service accessible depuis l'exterieur voit son nom dans les label. Ils sont tous sous la forme *service*.docker.localhost.

lancer avec la commande `docker compose up -d | docker-compose -f backup --no-start`

Lancer une première sauvegarde `docker start backup`
Lancer une prestoration `docker start restore`


## Parametrer le dashboard de grafana
Lors du premier lanement de grafana les identifiants/mot de passe de connexion son admin/admin. après la première connexion un nouveau mot de passe est demandé.

Dans Configuration/datasource il faut ajouter la source prometheus. 
![configuration de la data source prometheus](./content/grafana.png)

On peut ensuite créer son dashboard ou en importer un adapté au monitoring de conteneurs. Dans Create/import il suffit d'importer l'ID 193.
On obtient alors un dashboard fonctionnel.
![Dashboard monitoring de Conteneurs](./content/dashboard.png)
